<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assts/css/bootstrap.min.css">
    <script src="assts/js/jquery.min.js"></script>
    <script src="assts/js/bootstrap.min.js"></script>
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 70%;
            margin: auto;
        }
    </style>
</head>
</head>
<body>

<div class="container">
    <h2>Modal Example</h2>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="row">
                <div class="col-lg-12">
                    <form class="well" action="carousel.php" method="post" enctype="multipart/form-data">
                        <label for="file">Select a file to upload</label>
                        <input type="file" name="filetoupload" id="filetoupload">
                        <p class="help-block">jpg</p>

                        <div class="form-group">
                            Description:<br>
                            <input type="text" name="description" value="">
                            <br>
                            Date:<br>
                            <input type="date" name="date">
                            <br>

                        </div>

                        <input type="submit" class="btn btn-lg btn-primary" value="Upload">

                    </form>
                </div>


        </div>
    </div>

</div>

</body>
</html>

