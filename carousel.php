

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assts/css/bootstrap.min.css">
    <script src="assts/js/jquery.min.js"></script>
    <script src="assts/js/js/bootstrap.min.js"></script>
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 70%;
            margin: auto;
        }
    </style>
</head>
<body>
<?php

//var_dump($_FILES);
$img_nam=time().$_FILES['filetoupload']['name'];
$temp_location=$_FILES['filetoupload']['tmp_name'];
move_uploaded_file($temp_location,'picture/'.$img_nam);


//$image='picture/'.$img_nam;

//echo "<img src=$image>";


?>
<div class="container">
    <br>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

            <div class="item active">
                <img src="picture/<?php echo $img_nam?>" alt="Chania" width="460" height="345">
                <div class="carousel-caption">

                    <h3><?php echo $_POST['description']?> </h3>
                    <p><?php echo $_POST['date']?> </p>
                </div>
            </div>



        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

</body>
</html>